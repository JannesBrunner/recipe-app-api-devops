variable "prefix" {
  type        = string
  default     = "raad"
  description = "to prefix resources. raad = recipe app api devops"
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "basically the name of the project"
}

variable "contact" {
  type        = string
  default     = "mail@jannesbrunner.de"
  description = "contact info of devOp eng. in charge"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "022069299933.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "022069299933.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}