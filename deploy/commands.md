# Deploy commands
## Init terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform init

Init terraform on a new machine / project

## Format tf files 
docker-compose -f .\deploy\docker-compose.yml run --rm terraform fmt

format the *.tf files for better readability

## Validate terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform validate

Validating your terraform files

## Plan terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform plan

This will plan and show a summary of planned changes

## Apply terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform apply

This will apply changes, if any present

## Destroy terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform destroy

Gracefully destroy all allocated resources in the cloud

## List workspaces terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform workspace list

List all available workspaces. You can treat Workspaces like different environments.
Each environment has its own state!

## Add new workspace terraform
docker-compose -f .\deploy\docker-compose.yml run --rm terraform workspace new {name}

Create a new workspace called {name} e.g. terraform workspace new dev